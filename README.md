# TITLE

Description.

## Getting Started

### Prerequisites

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [python 3](https://docs.python.org/3/using/index.html)
* [scrapy](https://doc.scrapy.org/en/latest/intro/install.html)
* [shub](https://helpdesk.scrapinghub.com/support/solutions/articles/22000204081-deploying-your-spiders-to-scrapy-cloud) (optionally, for deployment to scrapinghub.com)

### Deployment Locally

Clone the repository and change directory:

```
git clone https://nsalikov@bitbucket.org/nsalikov/telekom_de.git
cd telekom_de
```

Change directory and install requirements:

```
pip install -r requirements.txt
```

If you have both python 2 and python 3 installed, use pip3 instead:

```
pip3 install -r requirements.txt
```

### Deployment to Scrapinghub.com

### Configuration
