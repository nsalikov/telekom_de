import io
import csv
import six
import xlsxwriter
from scrapy.conf import settings
from scrapy.exporters import CsvItemExporter
from scrapy.exporters import BaseItemExporter
from scrapy.utils.python import to_bytes, to_unicode, to_native_str, is_listlike


class FixCsvItemExporter(CsvItemExporter):

    def __init__(self, file, include_headers_line=True, join_multivalued=',', **kwargs):
        self.newline = settings.get('CSV_NEWLINE', '')

        fields_to_export = settings.getlist('FEED_EXPORT_FIELDS', [])
        if fields_to_export:
            kwargs['fields_to_export'] = fields_to_export

        self._configure(kwargs, dont_fail=True)
        if not self.encoding:
            self.encoding = 'utf-8'
        self.include_headers_line = include_headers_line

        file.seek(0)
        file.truncate(0)
        self.stream = io.TextIOWrapper(
            file,
            line_buffering=False,
            write_through=True,
            encoding=self.encoding,
            newline=self.newline
        ) if six.PY3 else file

        kwargs['delimiter'] = settings.get('CSV_DELIMITER', ',')
        kwargs['lineterminator'] = settings.get('CSV_LINETERMINATOR', '\n')

        self.csv_writer = csv.writer(self.stream, **kwargs)
        self._headers_not_written = True
        self._join_multivalued = join_multivalued


class XlsxItemExporter(BaseItemExporter):

    def __init__(self, file, fields_to_export=None, export_empty_fields=False, encoding='utf-8', indent=0):
        self.fields_to_export = fields_to_export
        self.export_empty_fields = export_empty_fields
        self.encoding = encoding
        self.indent = indent

        self.file = file
        self.file.seek(0)
        self.file.truncate(0)

        self.buffer = io.BytesIO()
        self.wb = xlsxwriter.Workbook(self.buffer, {'in_memory': True, 'strings_to_numbers': True, 'strings_to_formulas': True})
        self.ws = self.wb.add_worksheet()
        self.row_counter = 0

    def start_exporting(self):
        self.ws.write_row(self.row_counter, 0, self.fields_to_export)
        self.row_counter = self.row_counter + 1

    def export_item(self, item):
        fields = self._get_serialized_fields(item, default_value='', include_empty=True)
        values = list(self._build_row(x for _, x in fields))
        self.ws.write_row(self.row_counter, 0, values)
        self.row_counter = self.row_counter + 1

    def finish_exporting(self):
        self.wb.close()
        self.buffer.seek(0)
        self.file.write(self.buffer.read())

    def _get_serialized_fields(self, item, default_value=None, include_empty=None):
        """Return the fields to export as an iterable of tuples
        (name, serialized_value)
        """
        if include_empty is None:
            include_empty = self.export_empty_fields
        if self.fields_to_export is None:
            if include_empty and not isinstance(item, dict):
                field_iter = six.iterkeys(item.fields)
            else:
                field_iter = six.iterkeys(item)
        else:
            if include_empty:
                field_iter = self.fields_to_export
            else:
                field_iter = (x for x in self.fields_to_export if x in item)

        for field_name in field_iter:
            if field_name in item:
                field = {} if isinstance(item, dict) else item.fields[field_name]
                value = self.serialize_field(field, field_name, item[field_name])
            else:
                value = default_value

            yield field_name, value

    def _build_row(self, values):
        for s in values:
            try:
                yield to_native_str(s, self.encoding)
            except TypeError:
                yield s
