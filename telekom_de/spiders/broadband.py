# -*- coding: utf-8 -*-
import os
import csv
import json
import datetime
from urllib.parse import quote
import scrapy
from scrapy.exceptions import CloseSpider


class BroadbandSpider(scrapy.Spider):
    name = 'broadband'
    allowed_domains = ['telekom.de']
    url = 'https://ebs01.telekom.de/acproxy/bycaptcha.do'
    body = 'source=internet&ausbauinformationen=true&wholebuy=true&plz={Zipcode}&ort={City}&ortsteil=&strasse={Street}&hausnummer={House}&hausnummerzusatz='
    headers = {
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Accept-Language': 'en-US,en',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://www.telekom.de',
        'Referer': 'https://www.telekom.de/zuhause/tarife-und-optionen/internet',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
    }

    cwd = os.path.dirname(os.path.realpath(__file__))
    proxy_list = '../proxies.txt'
    proxy_list = os.path.join(cwd, proxy_list)

    # custom_settings = {
    #     # 'COOKIES_ENABLED': True,
    #     'DOWNLOADER_MIDDLEWARES': {
    #         'scrapy_fake_useragent.middleware.RandomUserAgentMiddleware': 80,
    #         'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
    #         'telekom_de.scrapy_proxies.RandomProxy': 100,
    #         'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
    #     },
    #     'RETRY_TIMES': 5,
    #     'RETRY_HTTP_CODES': [500, 503, 504, 400, 403, 404, 408],
    #     'PROXY_LIST': proxy_list,
    #     'PROXY_MODE': 2,
    #     'CUSTOM_PROXY': 'http://LOGIN:PASSWORD@de.proxymesh.com:31280',
    #     'RANDOM_UA_PER_PROXY': True,
    #     'RANDOM_UA_TYPE': 'desktop',
    #     'FAKEUSERAGENT_FALLBACK': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36',
    # }


    def start_requests(self):
        for row in self.get_addresses():
            body = self.body.format(Zipcode=quote(row['Zipcode']), City=quote(row['City']), Street=quote(row['Street']), House=quote(row['House']))

            yield scrapy.Request(self.url, method='POST', headers=self.headers, body=body, meta={'row': row})


    def parse(self, response):
        d = {}

        d['Zipcode'] = response.meta['row']['Zipcode']
        d['City'] = response.meta['row']['City']
        d['Street'] = response.meta['row']['Street']
        d['House'] = response.meta['row']['House']
        d['Date'] = datetime.datetime.now().date().isoformat()

        try:
            data = json.loads(response.text)
        except:
            print(response.text)
            raise CloseSpider("WTF?!")

        if 'maxDownstream' in data and data['maxDownstream']:
            d['Broadband'] = data['maxDownstream'] / 1000
        else:
            d['Broadband'] = '-'

        return d


    def get_addresses(self):
        csv_filename = None

        if hasattr(self, 'input'):
            csv_filename = self.input
        else:
            csv_filename = self.settings.get('CSV_IMPORT_FILENAME', None)

        if not csv_filename:
            raise CloseSpider("You must specificy CSV_IMPORT_FILENAME='addresses.csv' in settings.py or -a input=addresses.csv in command line")

        csv_fields = self.settings.getlist('CSV_IMPORT_FIELDS', ['Zipcode', 'City', 'Street', 'House'])
        skip_header = self.settings.getbool('CSV_IMPORT_SKIP_HEADER', True)
        csv_delimiter = self.settings.get('CSV_DELIMITER', ';')
        csv_lineterminator = self.settings.get('CSV_LINETERMINATOR', '\n')

        with open(csv_filename, encoding='utf8') as csvfile:
            dr = csv.DictReader(csvfile, fieldnames=csv_fields, delimiter=csv_delimiter, lineterminator=csv_lineterminator)

            if skip_header:
                next(dr)

            for row in dr:
                row['Zipcode'] = row['Zipcode'].strip()
                row['City'] = row['City'].strip()
                row['Street'] = row['Street'].strip()
                row['House'] = row['House'].strip()

                yield row
